package fr.sewatech.formation.csgroup.service;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MessageMemoryServiceTest {

  @Test
  void findAll_should_work() {
    // GIVeN
    MessageMemoryService service = new MessageMemoryService();
    service.messages.put("TEST", new JsonObject().put("id", "TEST"));

    // WHeN
    Future<JsonArray> future = service.findAll();

    // THeN
    assertThat(future.succeeded())
      .isTrue();
    assertThat(future.result().size())
      .isEqualTo(1);
  }
}
