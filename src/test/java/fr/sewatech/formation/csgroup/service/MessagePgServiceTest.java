package fr.sewatech.formation.csgroup.service;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class MessagePgServiceTest {

  @Test
  void findAll_should_work() {
    // GIVeN
    MessagePgService service = new MessagePgService(Vertx.vertx());

    // WHeN
    Future<JsonArray> future = service.findAll();

    // THeN
    assertThat(future.succeeded())
      .isTrue();
    assertThat(future.result().size())
      .isEqualTo(1);
  }
}
