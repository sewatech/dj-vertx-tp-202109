package fr.sewatech.formation.csgroup.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class MessageVerticle extends AbstractVerticle {

  private MessageService messageService;

  @Override
  public void start(Promise<Void> startPromise) {
//    messageService = new MessageSharedService(vertx);
    messageService = new MessagePgService(vertx);
    vertx.eventBus()
      .<JsonObject>consumer("app.message")
      .handler(this::handleMessage);
    messageService.load(".data/salut.txt")
      .onSuccess(nothing -> startPromise.complete())
      .onFailure(startPromise::fail);
  }

  private void handleMessage(Message<JsonObject> message) {
    JsonObject body = message.body();
    String action = message.headers().get("action");
    System.out.println("MessageVerticle - message received with action: " + action);
    Future<?> future = null;
    switch (action) {
      case "findAll":
        future = messageService.findAll();
        break;
      case "findById":
        future = messageService.findById(body.getString("id"));
        break;
      case "create":
        future = messageService.create(body.getString("text"));
        break;
      case "update":
        future = messageService.update(body.getString("id"), body.getJsonObject("document"));
        break;
      case "delete":
        future = messageService.delete(body.getString("id"));
        break;
    }
    future
      .onSuccess(message::reply)
      .onFailure(cause -> message.fail(500, cause.toString()));
  }

}
