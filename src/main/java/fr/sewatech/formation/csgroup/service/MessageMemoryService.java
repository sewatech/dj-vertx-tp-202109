package fr.sewatech.formation.csgroup.service;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MessageMemoryService implements MessageService {

  Map<String, JsonObject> messages = new HashMap<>();

  @Override
  public Future<JsonArray> findAll() {
    return Future.succeededFuture(new JsonArray(new ArrayList<>(messages.values())));
  }

  @Override
  public Future<JsonObject> findById(String id) {
    return Future.succeededFuture(messages.get(id));
  }

  @Override
  public Future<JsonObject> update(String id, JsonObject message) {
    if (messages.containsKey(id)) {
      messages.put(id, message);
      return Future.succeededFuture(message);
    } else {
      return Future.succeededFuture();
    }
  }

  @Override
  public Future<JsonObject> create(String text) {
    JsonObject newMessage = new JsonObject()
      .put("message", text)
      .put("id", UUID.randomUUID().toString())
      .put("timestamp", Long.toString(Instant.now().toEpochMilli()));
    messages.put(newMessage.getString("id"), newMessage);
    return Future.succeededFuture(newMessage);
  }

  @Override
  public Future<JsonObject> delete(String id) {
    return Future.succeededFuture(messages.remove("id"));
  }

  @Override
  public Future<Void> load(String path) {
    return null;
  }

}
