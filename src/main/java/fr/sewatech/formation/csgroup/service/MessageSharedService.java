package fr.sewatech.formation.csgroup.service;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.SharedData;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class MessageSharedService implements MessageService {

  private final SharedData sharedData;
  private final Vertx vertx;

  public MessageSharedService(Vertx vertx) {
    this.vertx = vertx;
    this.sharedData = this.vertx.sharedData();
  }

  @Override
  public Future<JsonArray> findAll() {
    return getMessages()
      .flatMap(AsyncMap::values)
      .map(JsonArray::new);
  }

  @Override
  public Future<JsonObject> findById(String id) {
    return getMessages()
      .compose(map -> map.get(id));
  }

  @Override
  public Future<JsonObject> update(String id, JsonObject message) {
    return getMessages()
      .compose(map -> map.replace(id, message))
      .map(replaced -> {
        if (replaced == null) {
          return null;
        } else {
          return message;
        }
      });
  }

  @Override
  public Future<JsonObject> create(String text) {
    String id = UUID.randomUUID().toString();
    JsonObject newMessage = new JsonObject()
      .put("message", text)
      .put("id", id)
      .put("timestamp", Long.toString(Instant.now().toEpochMilli()));
    return getMessages()
      .compose(map -> map.put(id, newMessage))
      .map(nothing -> newMessage);
  }

  @Override
  public Future<JsonObject> delete(String id) {
    return getMessages()
      .compose(map -> map.remove(id));
  }

  @Override
  public Future<Void> load(String path) {
    return vertx.fileSystem()
      .readFile(path)
      .compose(buffer -> {
        String content = buffer.toString();
        List<Future> futures = Arrays.stream(content.split("\\r?\\n"))
          .filter(line -> !line.isEmpty())
          .map(this::create)
          .collect(Collectors.toList());
        return CompositeFuture.all(futures)
          .map(unused -> null);
      });
  }

  private Future<AsyncMap<String, JsonObject>> getMessages() {
    return sharedData.getAsyncMap("message");
  }

}
