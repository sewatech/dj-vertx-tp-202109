package fr.sewatech.formation.csgroup.service;

import com.hazelcast.client.config.ClientNetworkConfig;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.micrometer.backends.BackendRegistries;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessagePgService implements MessageService {

  private final Vertx vertx;
  PgPool pool;

  public MessagePgService(Vertx vertx) {
    this.vertx = vertx;
  }

  @Override
  public Future<JsonArray> findAll() {
    return pool.query("SELECT id, text, created_at, updated_at FROM message")
      .mapping(this::toJson)
      .execute()
      .map(rowSet ->
        StreamSupport.stream(rowSet.spliterator(), false)
          .collect(Collector.of(JsonArray::new, JsonArray::add, JsonArray::addAll))
      );
  }

  @Override
  public Future<JsonObject> findById(String id) {
    return pool.preparedQuery("SELECT id, text, created_at, updated_at FROM message WHERE id=$1")
      .mapping(this::toJson)
      .execute(Tuple.of(Long.valueOf(id)))
      .map(rowSet -> rowSet.iterator().next());
  }

  @Override
  public Future<JsonObject> update(String id, JsonObject message) {
    message.put("updated_at", Instant.now().toEpochMilli());
    return pool.preparedQuery("UPDATE message set text=$2, updated_at=$3 WHERE id=$1")
      .execute(Tuple.of(Long.valueOf(id), message.getString("text"), message.getLong("updated_at")))
      .map(message);
  }

  @Override
  public Future<JsonObject> create(String text) {
    return pool.preparedQuery(
        "INSERT INTO message (text, created_at, updated_at) VALUES ($1, $2, $3) " +
          "RETURNING id, text, created_at, updated_at")
      .mapping(this::toJson)
      .execute(Tuple.of(
        text,
        Instant.now().toEpochMilli(),
        Instant.now().toEpochMilli()
      ))
      .map(rowSet -> rowSet.iterator().next());
  }

  @Override
  public Future<JsonObject> delete(String id) {
    return findById(id)
      .flatMap(
        result -> pool.preparedQuery("DELETE FROM message WHERE id=$1")
          .execute(Tuple.of(Long.valueOf(id)))
          .map(rows -> result)
      );
  }

  @Override
  public Future<Void> load(String path) {
    MeterRegistry registry = BackendRegistries.getDefaultNow();
    Counter counter = registry.counter("tp.message.loaded");

    return ConfigRetriever.create(vertx)
      .getConfig()
      .onSuccess(
        config -> {
          JsonObject dbConfig = config.getJsonObject("database");
          pool = PgPool.pool(
            vertx,
            new PgConnectOptions()
              .setHost(dbConfig.getString("host"))
              .setPort(dbConfig.getInteger("port"))
              .setDatabase(dbConfig.getString("database"))
              .setUser(dbConfig.getString("user"))
              .setPassword(dbConfig.getString("password")),
            new PoolOptions()
              .setMaxSize(5)
          );
        }
      )
      .flatMap(nothing -> pool.query("CREATE TABLE IF NOT EXISTS message (id SERIAL, text VARCHAR(255), created_at NUMERIC(32), updated_at NUMERIC(32))")
        .execute()
        .onSuccess(result -> System.out.println("Postgres OK"))
        .onSuccess(result -> vertx.fileSystem()
          .readFile(path)
          .compose(buffer -> {
            String content = buffer.toString();
            List<Future> futures = Arrays.stream(content.split("\\r?\\n"))
              .filter(line -> !line.isEmpty())
              .peek(line -> counter.increment())
              .map(this::create)
              .collect(Collectors.toList());
            return CompositeFuture.all(futures)
              .map(unused -> null);
          }))
        .onFailure(Throwable::printStackTrace)
        .map(unused -> null)
      );
  }

  private JsonObject toJson(Row row) {
    return new JsonObject()
      .put("id", row.getLong("id").toString())
      .put("text", row.getString("text"))
      .put("created_at", row.getLong("created_at"))
      .put("updated_at", row.getLong("updated_at"));
  }

}
