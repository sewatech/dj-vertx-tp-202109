package fr.sewatech.formation.csgroup.service;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public interface MessageService {

  Future<JsonArray> findAll();

  Future<JsonObject> findById(String id);

  Future<JsonObject> update(String id, JsonObject message);

  Future<JsonObject> create(String text);

  Future<JsonObject> delete(String id);

  Future<Void> load(String path);

}
