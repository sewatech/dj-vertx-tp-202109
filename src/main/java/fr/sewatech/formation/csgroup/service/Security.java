package fr.sewatech.formation.csgroup.service;

import io.vertx.config.ConfigRetriever;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.HashingStrategy;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authentication.AuthenticationProvider;
import io.vertx.ext.auth.authorization.RoleBasedAuthorization;
import io.vertx.ext.auth.sqlclient.SqlAuthentication;
import io.vertx.ext.auth.sqlclient.SqlAuthenticationOptions;
import io.vertx.ext.auth.sqlclient.SqlAuthorization;
import io.vertx.ext.auth.sqlclient.SqlAuthorizationOptions;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.AuthenticationHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Tuple;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashMap;

public class Security {

  private PgPool dbPool;

  public Future<Void> initDB(Vertx vertx) {
    return ConfigRetriever.create(vertx)
      .getConfig()
      .onSuccess(config -> {
        JsonObject connectionConfig = config
          .getJsonObject("postgres", new JsonObject())
          .getJsonObject("connection", new JsonObject());
        PgConnectOptions connectOptions = new PgConnectOptions()
          .setPort(connectionConfig.getInteger("port", 5432))
          .setHost(connectionConfig.getString("host", "localhost"))
          .setDatabase(connectionConfig.getString("database", "postgres"))
          .setUser(connectionConfig.getString("user", "postgres"))
          .setPassword(connectionConfig.getString("password", "tp-secret"));
        JsonObject poolConfig = config
          .getJsonObject("postgres", new JsonObject())
          .getJsonObject("pool", new JsonObject());
        PoolOptions options = new PoolOptions()
          .setMaxSize(poolConfig.getInteger("max-size", 5));
        dbPool = PgPool.pool(vertx, connectOptions, options);
      })
      .compose(unused ->
        dbPool.query("CREATE TABLE IF NOT EXISTS users (username VARCHAR(255), password VARCHAR(255))")
          .execute()
      )
      .compose(unused ->
        dbPool.query("DELETE FROM users")
          .execute()
      )
      .compose(unused ->
        dbPool.preparedQuery("INSERT INTO users (username, password) VALUES ($1, $2)")
          .execute(Tuple.of("user", hash("secret")))
      )
      .compose(unused ->
        dbPool.query("CREATE TABLE IF NOT EXISTS users_roles (username VARCHAR(255), role VARCHAR(255))")
          .execute()
      )
      .compose(unused ->
        dbPool.query("DELETE FROM users_roles")
          .execute()
      )
      .compose(unused ->
        dbPool.preparedQuery("INSERT INTO users_roles (username, role) VALUES ($1, $2)")
          .execute(Tuple.of("user", "msg"))
      )
      .compose(unused ->
        dbPool.query("CREATE TABLE IF NOT EXISTS roles_perm (role VARCHAR(255), perm VARCHAR(255))")
          .execute()
      )
//        .compose(unused ->
//            dbPool.preparedQuery("INSERT INTO roles_perm (role, perm) VALUES ($1, $2)")
//                .execute(Tuple.of("msg_role", "msg"))
//        )
      .map(unused -> null);
  }

  public AuthenticationHandler buildAuthenticationHandler() {
    SqlAuthenticationOptions options = new SqlAuthenticationOptions()
      .setAuthenticationQuery("SELECT password FROM users WHERE username = $1");
    AuthenticationProvider authnProvider =
      SqlAuthentication.create(dbPool, options);
    return BasicAuthHandler.create(authnProvider);
  }

  public Handler<RoutingContext> buildAuthorizationHandler() {
    return (RoutingContext routingContext) -> {
      User user = routingContext.user();
      SqlAuthorizationOptions options = new SqlAuthorizationOptions()
        .setRolesQuery("SELECT role FROM users_roles WHERE username = $1")
        .setPermissionsQuery("SELECT perm FROM roles_perm WHERE role = $1");
      SqlAuthorization.create(dbPool, options)
        .getAuthorizations(user)
        .onSuccess(nothing -> {
          if (RoleBasedAuthorization.create("msg")
            .match(user)) {
            routingContext.next();
          } else {
            routingContext.fail(403);
          }
        })
        .onFailure(throwable -> routingContext.fail(403, throwable));
    };
  }

  public static void main(String[] args) {
    String hash = hash("secret");
    System.out.println(hash);
  }

  private static String hash(String password) {
    SecureRandom random = new SecureRandom();
    byte[] rawSalt = new byte[48];
    random.nextBytes(rawSalt);
    String encodedSalt = Base64.getUrlEncoder().withoutPadding().encodeToString(rawSalt);

    return HashingStrategy.load()
      .hash("pbkdf2", new HashMap<>(), encodedSalt, password);
  }

}
