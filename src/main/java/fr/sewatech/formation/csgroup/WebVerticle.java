package fr.sewatech.formation.csgroup;

import fr.sewatech.formation.csgroup.service.Security;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.Cookie;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.PfxOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.SessionStore;
import io.vertx.micrometer.PrometheusScrapingHandler;

import java.time.Instant;

public class WebVerticle extends AbstractVerticle {

  private final Security security = new Security();

  @Override
  public void start(Promise<Void> startPromise) {
    security.initDB(vertx)
      .compose(unused -> {
        Router router = buildRouter();
        return CompositeFuture.all(
          httpSimple(router),
          httpTLS(router)
        );
      })
      .onSuccess(nothing -> startPromise.complete())
      .onFailure(startPromise::fail);
  }

  private Router buildRouter() {
    Router router = Router.router(vertx);

    router.route("/*")
      .handler(security.buildAuthenticationHandler())
      .handler(security.buildAuthorizationHandler());

    buildMessageRoutes(router);

    router.get("/hello")
      .produces("application/json")
      .handler(routingContext -> {
        vertx.eventBus()
          .<JsonObject>request("app.hello", "hello")
          .onSuccess(message ->
            routingContext.response()
              .putHeader("Content-Type", "application/json")
              .end(message.body().encode()))
          .onFailure(cause -> {
            System.out.println(cause.toString());
            routingContext.fail(500);
          });
      });

    router.get("/hello")
      .produces("text/plain")
      .handler(routingContext -> routingContext.end("Hello"));

    router.post("/hello")
      .consumes("text/plain")
      .handler(BodyHandler.create())
      .handler(routingContext -> {
        System.out.println("POST /hello: " + routingContext.getBodyAsString());
        routingContext.end("(post)");
      });

    router.route("/")
      .handler(routingContext -> routingContext.redirect("/hello"));

    router.get("/count")
      .handler(SessionHandler.create(SessionStore.create(vertx)))
      .handler(context -> context.response()
//            .end(countInSession(context.session()).toString())
          .end(countInCookie(context).toString())
      );

    router.get("/metrics")
      .handler(PrometheusScrapingHandler.create());

    router.errorHandler(404,
      context -> {
        if ("application/json".equals(context.request().getHeader("Accept"))) {
          context.response()
            .setStatusCode(404)
            .putHeader("Content-Type", "application/json")
            .end(
              new JsonObject()
                .put("message", "Resource not found")
                .put("timestamp", Instant.now().toEpochMilli())
                .encode()
            );
        } else {
          context.response().setStatusCode(404).end("Resource not found");
        }
      });

    return router;
  }

  private void buildMessageRoutes(Router router) {
    // All messages
    router.get("/message")
      .handler(routingContext ->
        vertx.eventBus()
          .<JsonArray>request(
            "app.message",
            new JsonObject(),
            new DeliveryOptions().addHeader("action", "findAll")
          )
          .onSuccess(reply -> routingContext.response()
            .putHeader("Content-Type", "application/json")
            .end(reply.body().encode()))
          .onFailure(cause -> routingContext.response().setStatusCode(404).end())
      );

    // One message, by id
    router.get("/message/:id")
      .handler(routingContext ->
        vertx.eventBus()
          .<JsonObject>request(
            "app.message",
            new JsonObject().put("id", routingContext.pathParam("id")),
            new DeliveryOptions().addHeader("action", "findById")
          )
          .onSuccess(reply -> {
            JsonObject body = reply.body();
            if (body == null) {
              routingContext.response().setStatusCode(404).end();
            } else {
              routingContext.response()
                .putHeader("Content-Type", "application/json")
                .end(body.encode());
            }
          })
          .onFailure(cause -> routingContext.response().setStatusCode(500).end())
      );

    // Update message, by id
    router.put("/message/:id")
      .handler(BodyHandler.create())
      .handler(routingContext ->
        vertx.eventBus()
          .<JsonObject>request(
            "app.message",
            new JsonObject()
              .put("id", routingContext.pathParam("id"))
              .put("document", routingContext.getBodyAsJson()),
            new DeliveryOptions().addHeader("action", "update")
          )
          .onSuccess(reply -> {
            JsonObject body = reply.body();
            if (body == null) {
              routingContext.response().setStatusCode(404).end();
            } else {
              routingContext.response()
                .putHeader("Content-Type", "application/json")
                .end(body.encode());
            }
          })
          .onFailure(cause -> routingContext.response().setStatusCode(500).end())
      );

    // Create message
    router.post("/message")
      .handler(BodyHandler.create())
      .handler(routingContext ->
        vertx.eventBus()
          .<JsonObject>request(
            "app.message",
            new JsonObject().put("text", routingContext.getBodyAsString()),
            new DeliveryOptions().addHeader("action", "create")
          )
          .onSuccess(reply -> routingContext.response()
            .putHeader("Content-Type", "application/json")
            .end(reply.body().encode()))
      );

    // Remove message, by id
    router.delete("/message/:id")
      .handler(routingContext ->
        vertx.eventBus()
          .<JsonObject>request(
            "app.message",
            new JsonObject().put("id", routingContext.pathParam("id")),
            new DeliveryOptions().addHeader("action", "delete")
          )
          .onSuccess(reply -> {
            JsonObject body = reply.body();
            if (body == null) {
              routingContext.response().setStatusCode(404).end();
            } else {
              routingContext.response()
                .putHeader("Content-Type", "application/json")
                .end(body.encode());
            }
          })
      );
  }

  private Future<Void> httpSimple(Router router) {
    return vertx.createHttpServer()
//      .requestHandler(req -> {
//        req.response()
//          .putHeader("content-type", "text/plain")
//          .end("Hello from Vert.x!");
//      })
      .requestHandler(router)
      .listen(8880)
      .onSuccess(server -> System.out.println("WebVerticle - HTTP server started on port " + server.actualPort()))
      .map(server -> null);
  }

  private Future<Void> httpTLS(Router router) {
    PfxOptions certOptions = new PfxOptions()
      .setPath(".config/server.p12")
      .setPassword("secret");
    HttpServerOptions options = new HttpServerOptions()
      .setUseAlpn(true)
      .setSsl(true)
      .setKeyCertOptions(certOptions)
//      .setHost("xxx")
      .setPort(8883);
    return vertx.createHttpServer(options)
//      .requestHandler(request -> request.response().end("Hello from TLS"))
      .requestHandler(router)
      .listen()
      .onSuccess(server -> System.out.println("WebVerticle - TLS Server started on port " + server.actualPort()))
      .map(server -> null);
  }

  private Long countInSession(Session session) {
    session.putIfAbsent("count", 0L);
    Long count = session.get("count");
    Long newCount = count + 1;
    session.put("count", newCount);
    return newCount;
  }

  private Long countInCookie(RoutingContext context) {
    Cookie cookie = context.getCookie("tp.count");
    if (cookie == null) {
      cookie = Cookie.cookie("tp.count", "0");
    }
    long count = Long.parseLong(cookie.getValue());
    Long newCount = count + 1;
    cookie.setValue(newCount.toString());
    context.addCookie(cookie);
    return newCount;
  }
}
