package fr.sewatech.formation.csgroup;

import fr.sewatech.formation.csgroup.service.MessageVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.micrometer.MicrometerMetricsOptions;
import io.vertx.micrometer.VertxJmxMetricsOptions;
import io.vertx.micrometer.VertxPrometheusOptions;

import java.lang.management.ManagementFactory;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Main {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    VertxOptions options = new VertxOptions();

    boolean debug = ManagementFactory
      .getRuntimeMXBean()
      .getInputArguments()
      .stream()
      .anyMatch(arg -> arg.startsWith("-agentlib:jdwp"));
    if (debug) {
      System.out.println("Main - Starting in debug mode");
      options.setBlockedThreadCheckInterval(1L)
        .setBlockedThreadCheckIntervalUnit(TimeUnit.DAYS);
    }

    MicrometerMetricsOptions micrometerOptions = new MicrometerMetricsOptions()
      .setEnabled(true)
      .setJmxMetricsOptions(
        new VertxJmxMetricsOptions()
          .setEnabled(true)
          .setDomain("vertx")
      )
//      .setPrometheusOptions(
//        new VertxPrometheusOptions()
//          .setEmbeddedServerOptions(
//            new HttpServerOptions().setPort(8889)
//          )
//          .setEmbeddedServerEndpoint("/metrics")
//          .setStartEmbeddedServer(true)
//          .setEnabled(true)
//          )
      ;
    options.setMetricsOptions(micrometerOptions);

    Vertx vertx = Vertx.vertx(options);

    vertx.deployVerticle(new WebVerticle())
      .onSuccess(id -> System.out.println("Main - Deployment WebVerticle OK"))
      .onFailure(Throwable::printStackTrace);

    vertx.deployVerticle(new HelloVerticle())
      .onSuccess(id -> System.out.println("Main - Deployment HelloVerticle OK"))
      .onFailure(Throwable::printStackTrace);

    JsonObject dbConfig = new JsonObject()
      .put("host", "127.0.0.1")
      .put("port", 5432)
      .put("database", "postgres")
      .put("user", "postgres")
      .put("password", "tp-secret");
    vertx.deployVerticle(
        MessageVerticle::new,
        new DeploymentOptions()
          .setInstances(1)
          .setConfig(new JsonObject().put("database", dbConfig)))
      .onSuccess(id -> System.out.println("Main - Deployment MessageVerticle OK"))
      .onFailure(Throwable::printStackTrace);


    Future<String> start = Future.succeededFuture("AZERTY");
    Future<Integer> result1 = start.map(text -> text.length());
    Future<Future<Integer>> result2 = start.map(text -> Future.succeededFuture(text.length()));
    Future<Integer> result3 = start.flatMap(text -> Future.succeededFuture(text.length()));

    Integer result = result3.toCompletionStage().toCompletableFuture().get();
    System.out.println(result);
  }

}
