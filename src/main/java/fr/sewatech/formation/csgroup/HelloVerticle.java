package fr.sewatech.formation.csgroup;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public class HelloVerticle extends AbstractVerticle {

  @Override
  public void start() {
    vertx.<Void>executeBlocking(promise -> {
      try {
        Thread.sleep(5000);
        promise.complete();
      } catch (InterruptedException e) {
        promise.fail(e);
      }
    });
    vertx.eventBus()
      .consumer("app.hello", message -> message.reply(
        new JsonObject()
          .put("message", "Hello")
          .put("request", message.body())
      ));
  }

}
